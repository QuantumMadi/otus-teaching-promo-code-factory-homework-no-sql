﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways
{
    public interface IPreferencesGateway
    {
        Task<ICollection<Preference>> GetPreferences();
        Task<Preference> GetByIdAsync(Guid id);
        Task<bool> UpdatePrefernceCache();
    }
}
