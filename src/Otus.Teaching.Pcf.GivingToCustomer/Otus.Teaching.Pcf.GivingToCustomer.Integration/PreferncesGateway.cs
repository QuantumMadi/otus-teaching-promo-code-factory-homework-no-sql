﻿using Newtonsoft.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class PreferncesGateway : IPreferencesGateway
    {
        private HttpClient _httpClient;
        public PreferncesGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<ICollection<Preference>> GetPreferences()
        {
            try
            {
                var defenition = new { source = "", preferences = new List<Preference>() };
                var response = await _httpClient.GetAsync("api/preference");
                var content = await response.Content.ReadAsStringAsync();
                var prefernces = JsonConvert.DeserializeAnonymousType(content, defenition);
                return prefernces.preferences;
            }
            catch (System.Exception ex)
            {
                return new List<Preference>();
                // TODO
            }

        }

        public async Task<Preference> GetByIdAsync(Guid id)
        {
            var response = await _httpClient.GetAsync($"/api/preference/{id}");
            var content = await response.Content.ReadAsStringAsync();
            var prefernce = JsonConvert.DeserializeObject<Preference>(content);
            return prefernce;
        }

        public async Task<bool> UpdatePrefernceCache()
        {
            var response = await _httpClient.GetAsync($"/api/preference/cache");
            var content = await response.Content.ReadAsStringAsync();
            var res = bool.TryParse(content, out bool result) ? result : false;
            return res;
        }
    }
}
