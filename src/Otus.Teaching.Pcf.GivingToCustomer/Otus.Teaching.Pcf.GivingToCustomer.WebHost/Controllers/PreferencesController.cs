﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Utils;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IPreferencesGateway _preferencesGateway;
        private readonly CacheService<Preference> _cacheService;

        public PreferencesController(IPreferencesGateway preferencesGateway, CacheService<Preference> cacheService)
        {
            _preferencesGateway = preferencesGateway;
            _cacheService = cacheService;
            _cacheService.CacheKey = "Prefernces";
        }

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            List<Preference> preferences = new List<Preference>();
            var prefFromCache = await _cacheService.GetDataAsync();
            string source = string.Empty;
            
            if (prefFromCache != null)
            {
                source = "Cache";
                preferences = prefFromCache.ToList();
            }
            else
            {
                source = "From prefernce Service";
                preferences = (await _preferencesGateway.GetPreferences()).ToList();
            }

            if (!preferences.Any())
            {
                return NoContent();
            }

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            return Ok(new { Source = source, Response = response });
        }
    }
}