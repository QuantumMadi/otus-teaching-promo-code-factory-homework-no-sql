﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teching.Pcf.Preferences.WebHost.DataAccess;
using Otus.Teching.Pcf.Preferences.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teching.Pcf.Preferences.WebHost.Controllers
{
    [Route("api/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private CacheService<Preference> _cacheService;
        private PreferencesContext _context;
        public PreferenceController(CacheService<Preference> cacheService, PreferencesContext context)
        {
            _cacheService = cacheService;
            _context = context;
            cacheService.CacheKey = "Prefernces";
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPreferences()
        {

            var preferences = await _cacheService.GetDataAsync();
            if (preferences != null)
            {
                return Ok(new { Source = "Cache", preferences });
            }
            preferences = await _context.Preferences.ToListAsync();

            await _cacheService.SetCacheAsync(preferences);
            return Ok(new { Source = "Database", preferences });

        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetPrefernceById(Guid id)
        {
            List<Preference> prefernces;
            Preference preference;
            prefernces = (await _cacheService.GetDataAsync()).ToList();
            if (prefernces != null)
            {
                preference = prefernces.FirstOrDefault(x => x.Id == id);
                return Ok(new { Source = "Cache", preference });
            }
            prefernces = await _context.Preferences.ToListAsync();
            await _cacheService.SetCacheAsync(prefernces);
            preference = prefernces.FirstOrDefault(x => x.Id == id);
            return Ok(new { Source = "Database", preference });
        }

        [HttpGet("cache")]
        public async Task<IActionResult> UpdateCache()
        {
            var prefernces = await _context.Preferences.ToListAsync();
            await _cacheService.SetCacheAsync(prefernces);
            return Ok(true);
        }
    }
}
