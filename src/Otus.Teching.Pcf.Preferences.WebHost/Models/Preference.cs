﻿using System;

namespace Otus.Teching.Pcf.Preferences.WebHost.Models
{
    public class Preference
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
