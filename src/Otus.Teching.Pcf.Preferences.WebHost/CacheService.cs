﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teching.Pcf.Preferences.WebHost
{
    public class CacheService<T>
    {
        private IDistributedCache _distributedCache;
        public string CacheKey { get; set; }
        public CacheService(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
            CacheKey = "DefaultKey";
        }

        public async Task<ICollection<T>> GetDataAsync()
        {
            try
            {
                var data = await _distributedCache.GetAsync(CacheKey);
                if (data.Length > 0)
                {
                    var jsonData = Encoding.UTF8.GetString(data);

                    var deserilizedData = JsonConvert
                            .DeserializeObject<ICollection<T>>(jsonData);

                    return deserilizedData;
                }
            }
            catch (Exception)
            {
                //TODO connection logging                         
            }
            return null;
        }

        public async Task SetCacheAsync(ICollection<T> data)
        {
            try
            {
                var json = JsonConvert.SerializeObject(data);
                var dataInBytes = Encoding.UTF8.GetBytes(json);
                await _distributedCache.SetAsync(CacheKey, dataInBytes);
            }
            catch (Exception)
            {
                //TODO connection logging       
            }
        }
    }
}
