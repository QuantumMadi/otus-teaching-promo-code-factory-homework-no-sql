﻿namespace Otus.Teching.Pcf.Preferences.WebHost.DataAccess
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}