﻿using Microsoft.EntityFrameworkCore;
using Otus.Teching.Pcf.Preferences.WebHost.Models;

namespace Otus.Teching.Pcf.Preferences.WebHost.DataAccess
{
    public class PreferencesContext : DbContext
    {
        public DbSet<Preference> Preferences { get; set; }
        public PreferencesContext(DbContextOptions<PreferencesContext> options)
            : base(options)
        {

        }
    }
}
