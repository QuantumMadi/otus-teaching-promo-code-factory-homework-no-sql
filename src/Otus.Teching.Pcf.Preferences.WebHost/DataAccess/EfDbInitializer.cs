﻿namespace Otus.Teching.Pcf.Preferences.WebHost.DataAccess
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly PreferencesContext _dataContext;

        public EfDbInitializer(PreferencesContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.AddRange(DataSeed.Preferences);
            _dataContext.SaveChanges();            
        }
    }
}